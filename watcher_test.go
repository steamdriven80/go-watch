package watcher

import (
	"fmt"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"testing"
	"time"
)

var baseDir = "/home/chris/Templates"
var fileExt = ".test"

func TestWatcher(t *testing.T) {

	watchMgr, err := NewWatcher(baseDir, fileExt)
	if err != nil {
		t.Error("main() -> ", err)
		return
	}

	watchMgr.Added = func(files []string) {
		fmt.Println("[Watcher.Added] -> files added: ", files)
	}

	watchMgr.Modified = func(files []string) {
		fmt.Println("[Watcher.Modified] -> files modified: ", files)
	}

	watchMgr.Removed = func(files []string) {
		fmt.Println("[Watcher.Removed] -> files removed: ", files)
	}

	if watchMgr != nil {
		fmt.Println("NewWatcher() created new object")
	}

	running := true

	for running == true {

		time.AfterFunc(20*time.Second, func(flip *bool) func() {
			return func() { *flip = !*flip }
		}(&running))

		var changeFS [3]func(root string)

		// this function will add a new log file
		changeFS[0] = func(root string) {
			os.Chdir(root)

			var name = "go_watcher_test" + strconv.FormatInt(rand.Int63(), 16) + fileExt

			os.Create(name)
		}

		// this function will modify a previously created test file
		changeFS[1] = func(root string) {
			name, _ := filepath.Glob(path.Join(root, "go_watcher_test*.log"))

			if len(name) == 0 {
				return
			}

			f, err := os.Open(name[0])

			if err != nil {

				return
			} else {
				f.WriteString("Herbity Dooperby Doo")
			}
		}

		changeFS[2] = func(root string) {
			os.Chdir(root)

			name, err := filepath.Glob(path.Join(root, "go_watcher_test*"+fileExt))
			if err != nil || len(name) == 0 {
				return
			}

			os.Remove(path.Join(root, name[0]))
		}

		//changeFS[rand.Int31()%3](baseDir)

		time.Sleep(1 * time.Second)

		_ = os.Stdout.Sync()
	}
}

func TestCleanup(t *testing.T) {
	os.Chdir(baseDir)

	name, err := filepath.Glob(path.Join(baseDir, "go_watcher_test*"+fileExt))
	if err != nil {
		return
	}

	for _, p := range name {
		fmt.Println("cleaning up -> deleting ", p)
		os.Remove(p)
	}
}
