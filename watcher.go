// :watcher project watcher.go
package watcher

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"time"
)

type Watcher struct {
	path      string
	extension string
	files     map[string]string

	stopWatching <-chan bool

	Added    FSCallback
	Removed  FSCallback
	Modified FSCallback
}

func stat2str(stats os.FileInfo) string {
	return fmt.Sprintf("%d%s%s", stats.Size(), stats.Name(), stats.Mode().String())
}

type FSCallback func([]string)

func subWatcher(obj *Watcher) error {

	fi, err := os.Stat(obj.path)

	if err != nil || fi.IsDir() != true {
		fmt.Errorf("%s", err)
		return err
	}

	// holds stat() objects for comparison of file changes
	fStats := make(map[string]string)

	// the query string we're searching for
	query := filepath.Join(obj.path, "*"+obj.extension)

	// the previous iterations file list
	pastList, _ := filepath.Glob(query)

	// helper objects
	added := make([]string, 0, 25)
	modified := make([]string, 0, 25)
	removed := make([]string, 0, 25)

	inSlice := func(container []string, item string) (bool, int) {
		for c, i := range container {
			if item == i {
				return true, c
			}
		}

		return false, 0
	}

	fmt.Println("QUERY: ", query)

	// until we're told to stop
	for {

		// grab the new file list
		newList, _ := filepath.Glob(query)

		// loop through all the new files
		for _, v := range newList {
			stats, _ := os.Stat(v)

			if fStats[v] != "" && fStats[v] != stat2str(stats) {
				modified = append(modified, v)
			}

			fStats[v] = stat2str(stats)
		}

		for _, v := range pastList {
			if in, _ := inSlice(newList, v); in == false {
				removed = append(removed, v)
			}
		}

		for _, v := range newList {
			if in, _ := inSlice(pastList, v); in == false {
				added = append(added, v)
			}
		}

		if obj.Added != nil && len(added) > 0 {
			obj.Added(added)
		}

		if obj.Modified != nil && len(modified) > 0 {
			obj.Modified(modified)
		}

		if obj.Removed != nil && len(removed) > 0 {
			obj.Removed(removed)
		}

		time.Sleep(500 * time.Millisecond)

		added = make([]string, 0)
		modified = make([]string, 0)
		removed = make([]string, 0)

		pastList = make([]string, len(newList))

		copy(pastList, newList)
	}
}

func NewWatcher(p string, ext string) (*Watcher, error) {

	that := &Watcher{path: p, extension: ext, files: make(map[string]string, 15)}

	if stat, err := os.Stat(p); err != nil && !stat.IsDir() {
		return &Watcher{}, errors.New("[error]: file does not exist!")
	}

	go subWatcher(that)

	return that, nil
}
